# Asset locations
set :css_dir,    'assets/stylesheets'
set :js_dir,     'assets/javascripts'
set :images_dir, 'assets/images'
set :fonts_dir,  'assets/fonts'
# set :build_dir,  '/Users/g/Dropbox/reflection/reflection-software/'

# Development
configure :development do
  activate :livereload
end

# Add bower to sprockets lookup path
after_configuration do
  sprockets.append_path File.join root.to_s, 'bower_components'
end

# Build settings
configure :build do
  # for dropbox, file based
  set :relative_links, true
  activate :relative_assets

  #activate :directory_indexes

  # cach busting
  activate :asset_hash

  ignore 'assets/images/*.psd'
  ignore 'assets/stylesheets/lib/*'
  ignore 'assets/stylesheets/vendor/*'
  ignore 'assets/stylesheets/site/*'
  ignore 'assets/javascripts/lib/*'
  ignore 'assets/javascripts/vendor/*'

  ignore   'README'

  if ENV['RACK_ENV'] == 'production'
    ignore   'robots.txt'
    activate :minify_css
    activate :minify_javascript
    activate :gzip
  end

  if ENV['RACK_ENV'] == 'staging'
    activate :minify_css
    activate :minify_javascript
    activate :gzip
  end



  # Change to your Google Analytics key (e.g. UA-XXXXX-Y)
  # To disable GA, leave unset or set to nil
  set :ga_key, nil


end


# use RackEnvironment, file: 'environment.yml'  if File.exist?('./environment.yml')

###
# Compass
###

# Change Compass configuration
# compass_config do |config|
#   config.output_style = :compact
# end

###
# Page options, layouts, aliases and proxies
###

# Per-page layout changes:
#
# With no layout
# page "/path/to/file.html", :layout => false
#
# With alternative layout
# page "/path/to/file.html", :layout => :otherlayout
#
# A path which all have the same layout
# with_layout :admin do
#   page "/admin/*"
# end

# Proxy pages (http://middlemanapp.com/basics/dynamic-pages/)
# proxy "/this-page-has-no-template.html", "/template-file.html", :locals => {
#  :which_fake_page => "Rendering a fake page with a local variable" }

###
# Helpers
###

# Automatic image dimensions on image_tag helper
# activate :automatic_image_sizes

# Reload the browser automatically whenever files change
# configure :development do
#   activate :livereload
# end

# Methods defined in the helpers block are available in templates
# helpers do
#   def some_helper
#     "Helping"
#   end
# end


# Build-specific configuration
# configure :build do
  # For example, change the Compass output style for deployment
  # activate :minify_css

  # Minify Javascript on build
  # activate :minify_javascript

  # Enable cache buster
  # activate :asset_hash

  # Use relative URLs
  # activate :relative_assets

  # Or use a different image path
  # set :http_prefix, "/Content/images/"
# end
